# Тест работы Git (с Bitbucket)


## Добавление в Git в PhpStorm

Alt+Insert -> .ignore -> ".gitignore file (Git)"

Прописать в .gitignore ``.idea/``

VCS -> Import Into Version Control -> Create Git Repository

Выделить основной каталог -> ``Ctrl+Alt+A`` (или ПКМ -> Git -> Add)


## Добавление в Bitbucket

[Создаем новый репозиторий на сайте](https://bitbucket.org/repo/create)

Добавляем его в локальный каталог

```
git remote add origin https://{UserName}@bitbucket.org/{RepoAdminName}/{reponame}.git
```

Пример:

```bash
git remote add origin https://Vadiok@bitbucket.org/Vadiok/gittest.git
```

Пушим в Bitbucket

```bash
git push -u origin --all
```

```bash
git push -u origin --tags
```

Коммитим изменеия: ``Ctrl+K`` (VCS -> Commit Changes)

* Message: ``Initial Commit``
* Commit and push


## Работа с ветками

### Создание новой ветки

Создание локальной ветки: Git -> New Branch

Или

```bash
git checkout master
```

### Взять изменения

Если необходимо взять новые изменения из какой-то ветки (в примере master)

```bash
git rebase master
```

### Объединить ветки

Объединяем текущую ветку с master

```bash
git merge master
```

### Удалить локальную ветку

```bash
git branch -d local_branch_name
```